document.addEventListener('DOMContentLoaded', () => {
  const addButton = document.getElementById('addTaskButton');
  const taskInput = document.getElementById('taskInput');
  const taskList = document.getElementById('taskList');

  addButton.addEventListener('click', () => {
    const task = taskInput.value.trim();
    if (task) {
      const listItem = document.createElement('li');
      listItem.innerHTML = `
        <div class="flex items-center justify-between bg-white p-2 rounded shadow mb-2">
          <span class="task-content">${task}</span>
          <div>
            <button class="complete-task mr-2 text-green-500">
              <i class="fas fa-check"></i>
            </button>
            <button class="delete-task text-red-500">
              <i class="fas fa-trash"></i>
            </button>
          </div>
        </div>
      `;
      taskList.appendChild(listItem);
      taskInput.value = '';

      listItem.querySelector('.complete-task').addEventListener('click', (e) => {
        e.target.closest('li').querySelector('.task-content').classList.toggle('line-through');
      });

      listItem.querySelector('.delete-task').addEventListener('click', (e) => {
        e.target.closest('li').remove();
      });
    }
  });
});
